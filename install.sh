#!/bin/bash
set -x


exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1 

echo ""
echo "........................................"
echo "Installation of application"
echo "........................................"
echo "Today's date: `date`"
echo "........................................"
echo ""
sudo apt-get install -y unzip
sudo apt update
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
aws --version
sudo apt-get install wget
echo 'Installing Java...'
aws s3 cp s3://deploy-app-servers/java/jdk-8u251-linux-x64.tar.gz /opt/java/
echo untar JAVA
sudo tar -xf /opt/java/jdk-8u251-linux-x64.tar.gz -C /opt/java/
sudo touch environment
echo "export JAVA_HOME=/opt/java/jdk1.8.0_251" >> environment
sudo mv -f environment /etc/
source /etc/environment
echo Set JAVA HOME
echo Completed installing java
echo Set JAVA HOME
echo 'Download Build Artifacts from S3'

# aws s3 cp s3://deploy-app-servers/xwiki/xwiki-platform-distribution-war-12.10.4.war /opt/xwiki/]

ARTIFACTS=xwiki-platform-distribution-war-12.10.4.war
TOMCAT=/opt/tomcat/9_0
TOMCAT_WEBAPPS=$TOMCAT/webapps
#WAR_FILE=$(curl -O http://52.55.80.222:8082/artifactory/Juwon/com/mkyong/JackPiro/1.0-SNAPSHOT/$ARTIFACTS.war)
WAR_FILE=$(aws s3 cp s3://deploy-app-servers/xwiki/build/$ARTIFACTS.war .)
TOMCAT_URL="https://downloads.apache.org/tomcat/tomcat-9/v9.0.44/bin/apache-tomcat-9.0.44.tar.gz"

function check_java_home {
    if [ -z ${JAVA_HOME} ]
    then
        echo 'Could not find JAVA_HOME. Please install Java and set JAVA_HOME'
	exit
    else 
	echo 'JAVA_HOME found: '$JAVA_HOME
        if [ ! -e ${JAVA_HOME} ]
        then
	    echo 'Invalid JAVA_HOME. Make sure your JAVA_HOME path exists'
	    exit
        fi
    fi
}

echo 'Installing tomcat server...'
echo 'Checking for JAVA_HOME...'
check_java_home

echo 'Downloading tomcat-9.0...'
if [ ! -f /etc/apache-tomcat-8*tar.gz ]
then
    curl -O $TOMCAT_URL
fi
echo 'Finished downloading...'

echo 'Creating install directories...'
sudo mkdir -p '/opt/tomcat/9_0'

if [ -d "/opt/tomcat/9_0" ]
then
    echo 'Extracting binaries to install directory...'
    sudo tar -xzf apache-tomcat-9*.tar.gz -C "/opt/tomcat/9_0" --strip-components=1
    echo 'Creating tomcat user group...'
    sudo groupadd tomcat
    sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
    
    echo 'Setting file permissions...'
    cd "/opt/tomcat/9_0"
    sudo chgrp -R tomcat "/opt/tomcat/9_0"
    sudo chmod -R g+r conf
    sudo chmod -R g+x conf

    # This should be commented out on a production server
    sudo chmod -R g+w conf

    sudo chown -R tomcat webapps/ work/ temp/ logs/

    echo 'Setting up tomcat service...'
    sudo touch tomcat.service
    sudo chmod 777 tomcat.service 
    echo "[Unit]" > tomcat.service
    echo "Description=Apache Tomcat Web Application Container" >> tomcat.service
    echo "After=network.target" >> tomcat.service

    echo "[Service]" >> tomcat.service
    echo "Type=forking" >> tomcat.service

    echo "Environment=JAVA_HOME=$JAVA_HOME" >> tomcat.service
    echo "Environment=CATALINA_PID=/opt/tomcat/9_0/temp/tomcat.pid" >> tomcat.service
    echo "Environment=CATALINA_HOME=/opt/tomcat/9_0" >> tomcat.service
    echo "Environment=CATALINA_BASE=/opt/tomcat/9_0" >> tomcat.service
    echo "Environment=CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC" >> tomcat.service
    echo "Environment=JAVA_OPTS=-Djava.awt.headless=true-Djava.security.egd=file:/dev/./urandom" >> tomcat.service

    echo "ExecStart=/opt/tomcat/9_0/bin/startup.sh" >> tomcat.service
    echo "ExecStop=/opt/tomcat/9_0/bin/shutdown.sh" >> tomcat.service

    echo "User=tomcat" >> tomcat.service
    echo "Group=tomcat" >> tomcat.service
    echo "UMask=0007" >> tomcat.service
    echo "RestartSec=10" >> tomcat.service
    echo "Restart=always" >> tomcat.service

    echo "[Install]" >> tomcat.service
    echo "WantedBy=multi-user.target" >> tomcat.service

    # echo 'Setting up tomcat manager..'
    # sudo echo "<role rolename="admin-gui"/>" >> /opt/tomcat/9_0/conf/tomcat-users.xml
    # sudo echo "<role rolename="manager-gui"/>" >> /opt/tomcat/9_0/conf/tomcat-users.xml
    # sudo echo "<user username="admin" password="admin-password" roles="admin-gui,manager-gui"/>" >> /opt/tomcat/9_0/conf/tomcat-users.xml
    # sudo echo "</tomcat-users>" >> /opt/tomcat/9_0/conf/tomcat-users.xml

    # sed -i 's/# autologin=dgod/autologin=ubuntu/' /path/to/file


    sudo mv tomcat.service /etc/systemd/system/tomcat.service
    sudo chmod 755 /etc/systemd/system/tomcat.service
    sudo systemctl daemon-reload
    
    echo 'Starting tomcat server....'
    sudo systemctl start tomcat
    exit
else
    echo 'Could not locate installation direcotry..exiting..'
    exit
fi

# This is deploying the WAR file

if [ ! -r $WAR_FILE ]; then
    echo "$WAR_FILE is missing. Download it and run this again to deploy it." 1>&2
else
    # Remove existing assets (if any)
    sudo rm -rf $TOMCAT_WEBAPPS/ROOT
    sudo cp $WAR_FILE $TOMCAT_WEBAPPS
fi
    sudo systemctl start tomcat

echo ""
echo "........................................"
echo "Installation of application"
echo "........................................"
echo "Today's date: `date`"
echo "........................................"
echo ""